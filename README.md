# disbalancer-cli (Docker, Alpine)

## How to build

`docker build -t disbalancer-cli .`

## How to run

`docker run --name disbalancer disbalancer-cli`
